DROP TABLE stats;

CREATE TABLE stats
(
    id SERIAL PRIMARY KEY NOT NULL,
    login_id int,
    site_id int,
    url text,
    activity text,
    application text,
    error int,
    ms int,
    family int,
    created timestamp,
    year_part text,
    month_part text,
    day_part text,
    ten_day_part text,
    hour_part text,
    ten_hour_part text,
    minute_part text,
    ten_minute_part text,
    second_part text,
    ten_second_part text
);

drop table login;
create table login
(
  id serial primary key not null,
  display_name text,
  given_name text,
  family_name text,
  email text,
  num_logins int,
  created timestamp,
  last_login timestamp,
  enabled boolean,
  identifier text
);
drop index login_identifier_idx;
create unique index login_identifier_idx ON login(identifier);

drop table site;
create table site
(
  id serial primary key not null,
  login_id int,
  url text,
  created timestamp
);