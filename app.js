/**
 * Up Shot app - site monitoring
 * Scott Umsted 4/2014
 * dependencies
 * nodejs
 * pg
 * express
 * passport
 * passport google
 * ejs
 * @type {exports}
 */
var express = require('express');
var user = require('./routes/user');
var pages = require('./routes/pages');
var loginHelper = require('./helpers/loginHelper');
var api = require('./routes/api')
var http = require('http');
var path = require('path');
var passport = require('passport');
var GoogleStrategy = require('passport-google').Strategy;

// load environment specific config json from UPSHOT_CONFIG system variable
var upShotConfig = require(process.env.UPSHOT_CONFIG);

// configure authentication in passport
passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(obj, done) {
  done(null, obj);
});
passport.use(new GoogleStrategy(
    {
        returnURL: upShotConfig.googleReturnUrl,
        realm: upShotConfig.googleRealm
    },
    function(identifier, profile, done){
        profile.identifier = identifier;
        return done(null, profile);
    }
));

// establish application ns
var app = express();

// all environments
app.set('port', upShotConfig.port);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.cookieParser());
app.use(express.methodOverride());
app.use(express.session({secret:upShotConfig.secret}));
app.use(passport.initialize());
app.use(passport.session());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

// Google stuff
app.get('/auth/google',
    passport.authenticate('google', { failureRedirect: '/' }),
    function(req, res) {
        res.redirect('/dashboard');
    }
);

app.get('/auth/google/return',
    passport.authenticate('google', { failureRedirect: '/' }),
    function(req, res) {
        loginHelper.upsertLogin(req);
        res.redirect('/dashboard');
    }
);

function ensureAuthenticated(req, res, next) {
    if (req.isAuthenticated()) { return next(); }
    res.redirect('/')
}

function ensureApiAuthenticated(req, res, next) {
    if (req.isAuthenticated()) { return next(); }
    res.send({'status':'unauthorized'});
}

function ensureCollectorAuthenticated(req, res, next) {
    if (req.headers.accesstoken == upShotConfig.collectorKey) { return next(); }
    res.send({'status':'unauthorized'});
}

// page routes
app.get('/', pages.index);
app.get('/signout', pages.signout);
app.get('/dashboard', ensureAuthenticated, pages.dashboard);

// api routes
app.get('/activity/id/:id', ensureApiAuthenticated, api.findById);
app.get('/activity/chart/:created',ensureApiAuthenticated, api.chart);
app.get('/activity/range/:siteId/:startTime/:stopTime', ensureApiAuthenticated, api.range);
app.get('/activity/family/:family',ensureApiAuthenticated, api.findByFamily);
app.get('/activity/since/:created',ensureApiAuthenticated, api.findSinceSeconds);
app.get('/sites', ensureApiAuthenticated, api.getSites);
app.post('/site', ensureApiAuthenticated, api.addSite);
app.delete('/site/:id', ensureApiAuthenticated, api.deleteSite);
app.get('/users', ensureApiAuthenticated, user.list);

// used by collector only
// todo: add key
app.get('/allSites', ensureCollectorAuthenticated, api.getAllSites);
app.post('/activity', ensureCollectorAuthenticated, api.addActivity);
//app.delete('/activity/:hours', ensureCollectorAuthenticated, api.purgeActivity);
app.delete('/activity/:hours', api.purgeActivity);

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});