var rest = require('restler');
var upShotConfig = require(process.env.UPSHOT_CONFIG);

console.log('collect site data');

var collector = {
    delay : upShotConfig.collectorDelay,

    purgeHours : upShotConfig.purgeHours,

    go : function(){
        var self = this;
        rest.get(upShotConfig.sitesUrl,{headers:{accesstoken:upShotConfig.collectorKey}})
            .on('complete',
                function(sites, response){
                    if(sites.length > 0){
                        for(var i in sites){
                            self.collect(sites[i], upShotConfig.collectorTimes, self.sayDone);
                        }
                    } else {
                        console.log("go: no sites");
                    }
                }
            )
            .on('error',
                function(data, response){
                    console.log("go: error /sites");
                }
            );
        this.purge();
    },

    purge : function(){
        var self = this;
        rest.del('/activity/'+this.purgeHours).on('complete',function(){
                console.log("purge: hours: "+self.purgeHours+" success");
            }).on('error',function(){
                console.log("purge: hours: "+self.purgeHours+" failure");
            });

    },

    sayDone : function(){
        console.log("looks like we're done");
    },

    collect : function(site, numLeft, finishFunc){
        var self = this;
        if(numLeft == 0){
            finishFunc();
        } else {
            setTimeout(function(site, numLeft, finishFunc){
                var activity = "check";
                var application = site.url;
                var ms = 0;
                var error = 0;
                var family = 0;
                var start = new Date();
                var url = site.url;
//                rest.get(url,{headers:{accesstoken:upShotConfig.collectorKey}})
                rest.get(url,{})
                    .on('complete',
                    function(data, response){
                        if(response != null){
                            var stop = new Date();
                            ms = stop - start;
                            console.log(url+":"+response.statusCode);
                            if(response.statusCode!=null && response.statusCode==200){
                                error = 0;
                            } else {
                                error = 1;
                            }
                            var data =  {"activity":activity,"application":application,"ms":ms,"error":error,"family":family,
                                "site_id": site.id, "url": url, "login_id": site.login_id};
                            //console.log(data);
                            rest.post(upShotConfig.storeUrl, {
                                data: data, headers:{accesstoken:upShotConfig.collectorKey}
                            }).on('complete', function(data, response) {
                                console.log("stored: numLeft:"+numLeft+" activity:"+activity+" ms:"+ms+" error:"+error+" family:"+family);
                            });
                        }
                    })
                    .on('error',
                    function(data, response){
                        var stop = new Date();
                        ms = stop - start;
                        error = 1;
                        var data = {"activity":activity,"application":application,"ms":ms,"error":error,"family":family,
                                "site_id": site.id, "url": url, "login_id": site.login_id};
                        console.log(data);

                        rest.post(upShotConfig.storeUrl, {
                            data: data, headers:{accesstoken:upShotConfig.collectorKey}
                            }).on('complete', function(data, response) {
                            console.log("stored: numLeft:"+numLeft+" activity:"+activity+" ms:"+ms+" error:"+error+" family:"+family);
                        });
                    });
                self.collect(site, numLeft-1, finishFunc);
            }, upShotConfig.collectorDelay, site, numLeft, finishFunc);
        }
    },

    rand : function(max){
        return Math.floor((Math.random()*(max-1)));
    }

}

collector.go();