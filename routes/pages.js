
exports.index = function(req, res){
    res.render('index', { user: req.user });
}

exports.signout = function(req,res){
        req.logout();
        req.session.destroy();
        res.redirect('/');
    }

exports.dashboard = function(req, res){
    res.render('dashboard', { user: req.user });
}
