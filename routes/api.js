var upShotConfig = require(process.env.UPSHOT_CONFIG);
var pg = require('pg');

String.prototype.lpad = function(padString, length) {
    var str = this;
    while (str.length < length)
        str = padString + str;
    return str;
}

String.prototype.rpad = function(padString, length) {
    var str = this;
    while (str.length < length)
        str = str + padString;
    return str;
}

exports.findById = function(req, res){
    var sql = "select s.id, s.activity, s.application, s.ms, s.error, s.family, s.created " +
        "from login l, stats s where l.identifier=$2 and l.enabled=true and s.login_id=l.id and s.id=$1 order by s.id";

    var identifier = req.user.identifier;

    var id = req.params.id;

    pg.connect(upShotConfig.databaseUrl, function(err, client, done) {
        var handleError = function(err) {
            if(!err) return false;
            done(client);
            res.send([]);
            return true;
        };
        client.query(sql,[id, identifier],function(err, result){
            if(handleError(err)) return;
            done(client);
            res.send(result.rows);
        });
    });
}

exports.findByFamily = function(req, res){
    var sql = "select s.id, s.activity, s.application, s.ms, s.error, s.family, s.created "+
        "from login l, stats s where l.identifier=$2 and l.enabled=true and s.login_id=l.id and s.family=$1 order by s.id";

    var family = req.params.family;

    pg.connect(upShotConfig.databaseUrl, function(err, client, done) {
        var handleError = function(err) {
            if(!err) return false;
            done(client);
            res.send([]);
            return true;
        };
        client.query(sql,[family],function(err, result){
            if(handleError(err)) return;
            done(client);
            res.send(result.rows);
        });
    });
}


exports.findSinceSeconds = function(req, res){
    var sql = "select id, activity, application, ms, error, family, created from stats where created >= $1 order by id";

    var createdString = req.params.created;

    var created = new Date(createdString.slice(0,4), createdString.slice(4,6)-1,
        createdString.slice(6,8), createdString.slice(8,10),
        createdString.slice(10,12), createdString.slice(12,14), 0);

    pg.connect(upShotConfig.databaseUrl, function(err, client, done) {
        var handleError = function(err) {
            if(!err) return false;
            done(client);
            res.send([]);
            return true;
        };
        client.query(sql,[created],function(err, result){
            if(handleError(err)) return;
            done(client);
            res.send(result.rows);
        });
    });
}

Date.prototype.addHours= function(h){
    this.setHours(this.getHours()+h);
    return this;
}

exports.purgeActivity = function(req, res){
    var sql = "delete from stats where created < $1 ";
    var hours = req.params.hours;
    var created = new Date().addHours(-hours);

//    var created = new Date(createdString.slice(0,4), createdString.slice(4,6)-1,
//        createdString.slice(6,8), createdString.slice(8,10),
//        createdString.slice(10,12), createdString.slice(12,14), 0);

    pg.connect(upShotConfig.databaseUrl, function(err, client, done) {
        var handleError = function(err) {
            if(!err) return false;
            done(client);
            res.send([]);
            return true;
        };
        client.query(sql,[created],function(err, result){
            if(handleError(err)) return;
            done(client);
            res.send({"status":"success"});
        });
    });
}

function craftTime(sTime){
    return new Date(sTime.slice(0,4), sTime.slice(4,6)-1,
        sTime.slice(6,8), sTime.slice(8,10),
        sTime.slice(10,12), sTime.slice(12,14), 0);
}

exports.range = function(req, res){
//    var sql = "select s.ten_minute_part as part,count(s.*) as count,to_char(avg(s.ms)/1000,'9999D99') as average, sum(s.error) as errors "+
//            "from login l, stats s where l.identifier=$3 and l.enabled=true and s.login_id=l.id and s.site_id=$4 and "+
//            "s.created >= $1 and s.created <= $2 group by s.ten_minute_part order by part";

    var sql = "select s.minute_part as part,count(s.*) as count,to_char(avg(s.ms)/1000,'9999D99') as average, sum(s.error) as errors "+
            "from login l, stats s where l.identifier=$3 and l.enabled=true and s.login_id=l.id and s.site_id=$4 and "+
            "s.created >= $1 and s.created <= $2 group by s.minute_part order by part";

//    var startTime = craftTime(req.params.startTime);
//    var stopTime = craftTime(req.params.stopTime);
    var startTime = new Date(req.params.startTime);
    var stopTime = new Date(req.params.stopTime);
    var siteId = req.params.siteId;
    var identifier = req.user.identifier;
//    var identifier = 'https://www.google.com/accounts/o8/id?id=AItOawmn_ZCAVOlMnURbSaeGw0UOdoChN7A1O30';//req.user.identifier;

    pg.connect(upShotConfig.databaseUrl, function(err, client, done) {
        var handleError = function(err) {
            if(!err) return false;
            console.log("chart: error: "+err);
            done(client);
            res.send([]);
            return true;
        };
        client.query(sql,[startTime, stopTime, identifier, siteId],function(err, result){
            if(handleError(err)) return;
            done(client);
            var summary = [];
            for(var i in result.rows){
                var dateString = result.rows[i].part.rpad("0",14);
                    dateString = dateString.slice(0,4)+"/"+dateString.slice(4,6)+"/"+
                                dateString.slice(6,8)+" "+dateString.slice(8,10)+":"+
                                dateString.slice(10,12)+":"+dateString.slice(12,14);
                summary[i] = {
                    "ts": dateString,
                    "average":Number(result.rows[i].average),
                    "count":Number(result.rows[i].count),
                    "errors":Number(result.rows[i].errors)
                }
            }
            res.send(summary);
        });
    });
}

exports.chart = function(req, res){
    var sql = " as part,count(s.*) as count,to_char(avg(s.ms)/1000,'9999D99') as average, sum(s.error) as errors "+
            "from login l, stats s where l.identifier=$2 and l.enabled=true and s.login_id=l.id and s.created >= $1 group by ";

    // todo: redo this nonsense
    var createdString = req.params.created;
    var identifier = req.user.identifier;

    var created = new Date(createdString.slice(0,4), createdString.slice(4,6)-1,
        createdString.slice(6,8), createdString.slice(8,10),
        createdString.slice(10,12), createdString.slice(12,14), 0);

    var now = new Date();
    var fromNow = Math.abs(created - now);
    var grouper = '';
    if(fromNow > 31104000000){
        grouper = 's.year_part';
    }else if(fromNow < 32140800000 && fromNow >= 2678400000){
        grouper = 's.month_part';
    }else if(fromNow < 2678400000 && fromNow >= 86400000){
        grouper = 's.day_part';
    }else if(fromNow < 86400000 && fromNow >= 3600000){
        grouper = 's.hour_part';
    }else if(fromNow < 3600000 && fromNow >= 60000){
        grouper = 's.minute_part';
    }else{
        grouper = 's.second_part';
    }
    grouper = 's.ten_minute_part';
    sql = "select "+grouper+sql+grouper+" order by part";

    pg.connect(upShotConfig.databaseUrl, function(err, client, done) {
        var handleError = function(err) {
            if(!err) return false;
            console.log("chart: error: "+err);
            done(client);
            res.send([]);
            return true;
        };
        client.query(sql,[created,identifier],function(err, result){
            if(handleError(err)) return;
            done(client);
            var summary = [];
            for(var i in result.rows){
                var dateString = result.rows[i].part.rpad("0",14);
                    dateString = dateString.slice(0,4)+"/"+dateString.slice(4,6)+"/"+
                                dateString.slice(6,8)+" "+dateString.slice(8,10)+":"+
                                dateString.slice(10,12)+":"+dateString.slice(12,14);
                summary[i] = {
                    "date": dateString,
                    "average":result.rows[i].average,
                    "count":result.rows[i].count,
                    "errors":result.rows[i].errors
                }
            }
            res.send(summary);
        });
    });
}


// these are only used by collector locally and with a key
exports.addSite = function(req, res){
    var loginSql = "select id from login where identifier=$1 and enabled=true";
    var insertSql = "insert into site (login_id, url, created) " +
        "values ($1, $2, now())";

    var site = req.body;
    var identifier = req.user.identifier;

    pg.connect(upShotConfig.databaseUrl, function(err, client, done) {
        var handleError = function(err) {
            if(!err) return false;
            done(client);
            res.send([]);
            return true;
        };
        client.query(loginSql,[identifier],
            function(err, result){
                if(handleError(err)) return;
                if(result.rows.length>0){
                    for(var i in result.rows){
                        client.query(insertSql,[result.rows[i].id, site.url],
                            function(err, result){
                                done(client);
                                res.send({'status':'success'});
                            }
                        );
                    }
                }else{
                    res.send({'status':'success'});
                }
            }
        );
    });
}

// these are only used by collector locally and with a key
exports.deleteSite = function(req, res){
    var loginSql = "select id from login where identifier=$1 and enabled=true";
    var deleteSql = "delete from site where login_id=$1 and id=$2";

    var siteId = req.params.id;
    var identifier = req.user.identifier;

    pg.connect(upShotConfig.databaseUrl, function(err, client, done) {
        var handleError = function(err) {
            if(!err) return false;
            done(client);
            res.send([]);
            return true;
        };
        client.query(loginSql,[identifier],
            function(err, result){
                if(handleError(err)) return;
                if(result.rows.length>0){
                    for(var i in result.rows){
                        client.query(deleteSql,[result.rows[i].id, siteId],
                            function(err, result){
                                done(client);
                                res.send({'status':'success'});
                            }
                        );
                    }
                }else{
                    res.send({'status':'success'});
                }
            }
        );
    });
}

exports.getSites = function(req, res){
    var sql = "select s.id as id, s.url as url from site s, login l where l.identifier=$1 and l.enabled=true and s.login_id=l.id";
    var identifier = req.user.identifier;

    pg.connect(upShotConfig.databaseUrl, function(err, client, done) {
        var handleError = function(err) {
            if(!err) return false;
            done(client);
            res.send([]);
            return true;
        };
        client.query(sql,[identifier],function(err, result){
            if(handleError(err)) return;
            done(client);
            res.send(result.rows);
        });
    });

}

// these are only used by collector locally and with a key
exports.addActivity = function(req, res){
    var sql = "insert into stats (activity, application, ms, error, family, created, " +
        "year_part, month_part, day_part, hour_part, minute_part, second_part, ten_day_part, ten_hour_part, ten_minute_part, ten_second_part, " +
        "login_id,site_id,url) " +
        "values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19)";

    var activity = req.body;

    var created = new Date();
    var yearPart = "" + created.getFullYear().toString().lpad("0",4);
    var monthPart = yearPart + (created.getMonth()+1).toString().lpad("0",2);
    var dayPart = monthPart + created.getDate().toString().lpad("0",2);
    var tenDayPart = dayPart.slice(0,dayPart.length-1);
    var hourPart = dayPart + created.getHours().toString().lpad("0",2);
    var tenHourPart = hourPart.slice(0,hourPart.length-1);
    var minutePart = hourPart + created.getMinutes().toString().lpad("0",2);
    var tenMinutePart = minutePart.slice(0,minutePart.length-1);
    var secondsPart = minutePart + created.getSeconds().toString().lpad("0",2)
    var tenSecondPart = secondsPart.slice(0,secondsPart.length-1);

    // push
    console.log(upShotConfig.databaseUrl);
    pg.connect(upShotConfig.databaseUrl, function(err, client, done) {
        var handleError = function(err) {
            if(!err) return false;
            done(client);
            res.send([]);
            return true;
        };
        client.query(sql,[activity.activity, activity.application, activity.ms, activity.error, activity.family,
                            created, yearPart, monthPart, dayPart, hourPart, minutePart, secondsPart,
                            tenDayPart, tenHourPart, tenMinutePart, tenSecondPart, activity.login_id,
                            activity.site_id, activity.url],
                            function(err, result){
            if(handleError(err)) return;
            done(client);
            res.send({'status':'success'})
        });
    });
}

// todo add key
exports.getAllSites = function(req, res){
    var sql = "select id, login_id , url from site";

    pg.connect(upShotConfig.databaseUrl, function(err, client, done) {
        var handleError = function(err) {
            if(!err) return false;
            done(client);
            res.send([]);
            return true;
        };
        client.query(sql,[],function(err, result){
            if(handleError(err)) return;
            done(client);
            res.send(result.rows);
        });
    });

}