## Synopsis

http://upshot.ezpz.gs

Upshot is a site uptime and monitoring app. I wrote this to learn nodejs/express and to find a use for Square's cubism add on for D3js. The app uses postgresql for the datastore.

## Installation

There are a few things you'll need to do to run this after cloning.

1. Setup a json file outside of the project and assign it the system variable UPSHOT_CONFIG. The file should contain the following:


        {
        "databaseUrl": "postgres://localhost:5432/upshotdb",
        "sitesUrl": "http://localhost:3000/allSites",
        "storeUrl": "http://localhost:3000/activity",
        "addUrlSuffix": false,
        "port": 3000,
        "collectorTimes": 1000,
        "collectorDelay": 10000,
        "collectorKey": "key here",
        "googleReturnUrl" : "http://localhost:3000/auth/google/return",
        "googleRealm" : "http://localhost:3000",
        "secret" : "secret here",
        "purgeHours" : 24
        }


2. Setup a cron job to define the UPSHOT_CONFIG variable and to run the collector. I set collectorTimes to 1 and run the collector every minute.

3. The DDL is included in the sql directory. You'll need to run that against your postgresql db.

## License

MIT