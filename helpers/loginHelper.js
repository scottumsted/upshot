var upShotConfig = require(process.env.UPSHOT_CONFIG);
var pg = require('pg');

function ifn(value){
    if(typeof(value) != "undefined" && value != null){
        return value;
    }else{
        return "";
    }
}

exports.upsertLogin = function(req){
    var updateSql = "update login set last_login=now(), num_logins=num_logins+1 where identifier = $1 ";
    var insertSql = "INSERT INTO login (display_name, given_name, family_name, email, num_logins, created, last_login, enabled, identifier) " +
            "values ($1,$2,$3,$4,1,now(),now(),true,$5)";

    var params = [
        ifn(req.user.displayName),
        ifn(req.user.name.givenName),
        ifn(req.user.name.familyName),
        ifn(req.user.emails[0].value),
        ifn(req.user.identifier)
    ];

    pg.connect(upShotConfig.databaseUrl, function(err, client, done){
            var handleError = function(err) {
                if(!err) return false;
                done(client);
                console.log("upsertLogin: update fail: "+params+", error: "+err);
                return true;
            };
            client.query(updateSql,[req.user.identifier],function(err, result){
                if(handleError(err)) return;
                client.query(insertSql,params, function(err, result){
                    done(client);
                    console.log("upsertLogin: insert: "+params);
                });
            });
        }

    );
}