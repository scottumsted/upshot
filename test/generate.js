var rest = require('restler');

console.log('generate test data');

var generate = {
    numActivities : 500,
    delay : 2000,
    activities : ["login","create","track","detail","account","logout"],
    msBase : [1000, 5000, 2000, 3000, 1500, 500],

    go : function(){
        this.generate(this.numActivities, this.sayDone);
    },

    sayDone : function(){
        console.log("looks like we're done");
    },

    generate : function(numLeft, finishFunc){
        self = this;
        if(numLeft == 0){
            finishFunc();
        } else {
            setTimeout(function(){
                var index = self.rand(self.activities.length);
                var activity = self.activities[index];
                var ms = self.msBase[index] + self.rand(500);
                var error = self.rand(1);
                var family = self.rand(2);
                rest.post('http://localhost:3000/activity', {
                  data: {"activity":activity,"application":"app1","ms":ms,"error":error,"family":family},
                }).on('complete', function(data, response) {
                    console.log("stored: numLeft:"+numLeft+" activity:"+activity+" ms:"+ms+" error:"+error+" family:"+family);
                });
                console.log("numLeft:"+numLeft+" activity:"+activity+" ms"+ms);
                self.generate(numLeft-1, finishFunc);
            }, self.delay);
        }
    },

    rand : function(max){
        return Math.floor((Math.random()*(max-1)));
    }

}

generate.go();