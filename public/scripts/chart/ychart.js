/**
 * Created by scottumsted on 4/6/14.
 */
$(document).ready(function(){

    String.prototype.lpad = function(padString, length) {
        var str = this;
        while (str.length < length)
            str = padString + str;
        return str;
    }
    var created = new Date((new Date)*1 - (5*60*60*1000));
    var createdString = "" + created.getFullYear().toString().lpad("0",4) +
            (created.getMonth()+1).toString().lpad("0",2) +
            created.getDate().toString().lpad("0",2) +
            created.getHours().toString().lpad("0",2) +
            created.getMinutes().toString().lpad("0",2) +
            created.getSeconds().toString().lpad("0",2)
    var url = "/activity/summary/" + createdString;
    console.log("data url:"+url);
    $.ajax(url).done(
        function(data){
            $('.story-main').html('');
            //chart here
            YUI().use('charts', function (Y)
            {

                var myStyles = {
                   background: {
                      fill : {
                         color : "#FFFFFF"
                      },
                      border : {
                         color : "#dad8c9",
                         weight : 1
                      }
                   }
                };

                var myAxes = {
                    nums: {
                        type:"numeric",
                        position:"left",
                        labelFormat:{"decimalPlaces":1},
                        keys:["count","errors"]
                    },
                    durations: {
                        type:"numeric",
                        position:"right",
                        labelFormat:{"decimalPlaces":2},
                        keys:["average"]
                    },
                    datePart:{
                        keys:["date"],
                        position:"bottom",
                        type:"category",
                        styles:{
                            majorTicks:{
                                display: "none"
                            },
                            label: {
                                rotation:-45,
                                margin:{top:5},
                                fontSize:"50%"
                            }
                        }
                    }
                };

                var seriesCollection = [
                    {
                        type:"column",
                        xAxis:"datePart",
                        xKey:"date",
                        yKey:"count",
                        styles: {
                            fill: {
                                color: "#DDDDDD"
                                },
                            border: {
                                weight: 1,
                                color: "#DDDDDD"
                            },
                            over: {
                                fill: {
                                    alpha: 0.7
                                }
                            }
                        }
                    },
                    {
                        type:"column",
                        xAxis:"datePart",
                        xKey:"date",
                        yKey:"errors",
                        styles: {
                            marker:{
                                fill: {
                                    color: "#999"
                                },
                                border: {
                                    weight: 1,
                                    color: "#cbc8ba"
                                },
                                over: {
                                    fill: {
                                        alpha: 0.7
                                    }
                                }
                            }
                        }
                    },
                    {
                        type:"combo",
                        xKey:"date",
                        yKey:"average",
                        line: {
                            color: "#ff7200"
                        },
                        marker: {
                            fill: {
                                color: "#ff9f3b"
                            },
                            border: {
                                color: "#ff7200",
                                weight: 1
                            },
                            over: {
                                width: 12,
                                height: 12
                            },
                            width:9,
                            height:9
                        }
                    }
                ];


                var mychart = new Y.Chart({
                    dataProvider:data,
                    categoryKey:"date",
                    axes:myAxes,
                    render:"#upnowchart",
                    seriesCollection:seriesCollection,
                    styles: {
                        axes: {
                            date: {
                                label: {
                                    rotation: -45
                                }
                            }
                        }
                    },
                    horizontalGridlines: {
                        styles: {
                            line: {
                                color: "#dad8c9"
                            }
                        }
                    },
                    verticalGridlines: {
                        styles: {
                            line: {
                                color: "#dad8c9"
                            }
                        }
                    },
                    styles: {graph:myStyles}
                });
            });

        }
    ).fail(
        function(one,error,three){
            console.log(error);
        }
    );
});

