/**
 * Created by scottumsted on 5/17/14.
 */
var upshotChart = {
    sites : [],
    element : '',
    deleteSite : function(siteName,self){
        var usure = confirm("Remove "+siteName+"?");
        if(usure){
            var siteId = -1;
            for(var i=0;i<self.sites.length;i++){
                if(self.sites[i].url==siteName){
                    $.ajax({
                        url: '/site/'+self.sites[i].id,
                        type: 'DELETE',
                        success: function(result) {
                            $('#newSite').val('');
                            $(self.element).html('');
                            upshotChart.displayCharts(self);
                        }
                    });
                }
            }
        }
    },
    start : function(element){
        this.element = element;
        this.displayCharts(this);
    },
    displayCharts : function(self){
        context = cubism.context()
            .step(60 * 1000)
            .serverDelay(60 * 1000)
            .size(1024);
        context.colors = ["#006d2c","#31a354","#74c476","#bae4b3","#bdd7e7","#6baed6","#3182bd","#08519c"];

        d3.select(self.element).selectAll(".axis")
            .data(["top", "bottom"])
            .enter().append("div")
            .attr("class", function(d) { return d + " axis"; })
            .each(function(d) { d3.select(this).call(context.axis().ticks(12).orient(d)); });

        d3.select(self.element).append("div")
            .attr("class", "rule")
            .call(context.rule());

        d3.json('/sites',
            function(sites){
                self.sites = sites;
                d3.select(self.element).selectAll(".horizon")
                    .data(sites.map(function(site){
                            console.log(site);
                            return context.metric(function(start, stop, step, callback) {
                                var url = '/activity/range/'+site.id+'/'+start.toISOString()+'/'+stop.toISOString();
                                console.log(url);
                                d3.json(url,function(activity){
                                        var values = [];
                                        var k=0;
                                        var startMs = start.getTime();
                                        var stopMs = stop.getTime();
                                        var activityLength = activity.length;
                                        for(var j=startMs;j<=stopMs;j+=step){
                                            if(k<activityLength){
                                                var activityMs = (new Date(activity[k].ts)).getTime();
                                                if(activityMs<=j){
                                                    values.push(activity[k].average);
                                                    k++;
                                                }else{
                                                    values.push(0);
                                                }
                                            }else{
                                                values.push(0);
                                            }
                                        }
                                        callback(null,values);
                                    }
                                );
                            }, site.url);
                        }
                    ))
                  .enter().insert("div", ".bottom")
                    .attr("class", "horizon")
                    .call(context.horizon().height(50).extent([0,3]))
                    .on("click", function(obj){
                        self.deleteSite(obj.toString(),self);
                    });
            }
        );
        context.on("focus", function(i) {
          d3.selectAll(".value").style("right", i == null ? null : context.size() - i + "px");
        });

    }
};
upshotChart.start('#squarecharts');